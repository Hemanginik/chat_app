import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

var firebaseConfig = {
  apiKey: "AIzaSyAuw9jwc2ryBUQu0R3-Q7FOlQ3WlAbYxpI",
  authDomain: "react-slack-clone-ba9c9.firebaseapp.com",
  databaseURL: "https://react-slack-clone-ba9c9.firebaseio.com",
  projectId: "react-slack-clone-ba9c9",
  storageBucket: "react-slack-clone-ba9c9.appspot.com",
  messagingSenderId: "38191044833",
  appId: "1:38191044833:web:de7abde59073f3f18bbfa0",
  measurementId: "G-Q46KBR737Z"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
