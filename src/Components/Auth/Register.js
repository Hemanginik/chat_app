import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import md5 from "md5";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  Grid,
  Paper,
  InputAdornment,
  Typography,
  CssBaseline
} from "@material-ui/core";
import { Link } from "react-router-dom";
import "../../Components/App.css";
import AccountCircleSharpIcon from "@material-ui/icons/AccountCircleSharp";
import LockIcon from "@material-ui/icons/Lock";
import EmailIcon from "@material-ui/icons/Email";
import { deepPurple } from "@material-ui/core/colors";
import firebase from "../../firebase";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    height: "100vh",
    justifyContent: "center",
    alignContent: "center"
  },
  input: {
    display: "block",
    alignItems: "center",
    paddingBottom: theme.spacing(1)
  },
  paper: {
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    paddingBottom: theme.spacing(2),
    paddingTop: theme.spacing(3),
    width: theme.spacing(55),
    height: "auto",
    display: "flex",
    justifyContent: "space-evenly",
    flexDirection: "column"
  },
  typography: {
    marginTop: theme.spacing(1),
    textAlign: "center"
  },
  purple: {
    backgroundColor: deepPurple[800],
    width: theme.spacing(7),
    height: theme.spacing(7),
    alignSelf: "center"
  },
  title: {
    alignSelf: "center",
    fontWeight: 700,
    fontSize: 20,
    paddingBottom: theme.spacing(2.5),
    paddingTop: theme.spacing(1)
  },
  titleImage: {
    width: theme.spacing(8.7),
    height: theme.spacing(8.7),
    alignSelf: "center",
    textAlign: "center"
  },
  img: {
    height: "100%"
  }
}));

function Register() {
  const classes = useStyles();
  const [errorMessage, setErrorMessage] = useState("");
  const [open, setOpen] = React.useState(false);

  const [state, setState] = useState({
    userName: "",
    password: "",
    confirmPassword: "",
    email: ""
  });

  const [error, setError] = useState({
    userName: "",
    password: "",
    confirmPassword: "",
    email: ""
  });

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = e => {
    e.preventDefault();
    firebase
      .auth()
      .createUserWithEmailAndPassword(state.email, state.password)
      .then(createUser => {
        createUser.user.updateProfile({
          displayName: state.userName,
          photoURL: `http://gravatar.com/avatar/${md5(
            createUser.user.email
          )}?d=identicon`
        });
        console.log(createUser);
      })
      .catch(err => {
        console.log(err.message);
        setErrorMessage(err.message);
        setOpen(true);
      });
  };

  const handleNameValidation = e => {
    const value = e.target.value;
    const name = e.target.name;
    setState({ ...state, [name]: value });

    if (!value.match(/^[a-zA-Z]*$/)) {
      setError({ ...error, [name]: "Field must contain only alphabets" });
    } else {
      setError({ ...error, [name]: "" });
    }
  };

  const handleEmailValidation = e => {
    const value = e.target.value;
    setErrorMessage("");
    setState({ ...state, email: value });
    if (value === "") {
      return setError({ ...error, email: "" });
    }
    if (!value.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      setError({ ...error, email: "Not Valid Email" });
    } else {
      setError({ ...error, email: "" });
    }
  };

  const handlePasswordValidation = e => {
    const value = e.target.value;
    setState({ ...state, password: value });
    if (value === "") {
      return setError({ ...error, password: "" });
    }

    if (
      !value.match(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/
      )
    ) {
      setError({ ...error, password: "Password is Invalid" });
    } else {
      setError({ ...error, password: "" });
    }
  };

  const handleConfirmPasswordValidation = e => {
    const value = e.target.value;
    setState({ ...state, confirmPassword: value });

    if (value === "") {
      return setError({ ...error, confirmPassword: "" });
    }
    if (state.password === value) {
      setError({ ...error, confirmPassword: "" });
    } else {
      setError({ ...error, confirmPassword: "Field must match password" });
    }
  };

  const handleChange = e => {
    switch (e.target.name) {
      case "userName": {
        return handleNameValidation(e);
      }
      case "email": {
        return handleEmailValidation(e);
      }
      case "password": {
        return handlePasswordValidation(e);
      }
      case "confirmPassword": {
        return handleConfirmPasswordValidation(e);
      }
      default:
        return null;
    }
  };

  return (
    <>
      <CssBaseline />
      {console.log(errorMessage)}
      <Grid className={classes.root} alignItems="stretch" container>
        <Grid item>
          <Paper elevation={6} className={classes.paper}>
            <Icon fontSize="large" className={classes.titleImage}>
              <img alt="logo" src="login.svg" className={classes.img} />
            </Icon>
            <Typography className={classes.title}>Register</Typography>
            <form onSubmit={handleSubmit} autoComplete="off">
              <div>
                <TextField
                  fullWidth
                  required
                  error={error.userName !== "" ? true : false}
                  className={classes.input}
                  label="User Name"
                  name="userName"
                  variant="outlined"
                  value={state.userName}
                  helperText={
                    error.userName !== ""
                      ? "Field must contain only alphabets"
                      : null
                  }
                  size="small"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <AccountCircleSharpIcon />
                      </InputAdornment>
                    )
                  }}
                  onChange={e => handleChange(e)}
                />

                <TextField
                  required
                  fullWidth
                  className={classes.input}
                  label="Email"
                  size="small"
                  name="email"
                  variant="outlined"
                  margin="normal"
                  helperText={error.email !== "" ? "Email Id is Invalid" : null}
                  value={state.email}
                  error={
                    error.email !== ""
                      ? true
                      : false || errorMessage
                      ? true
                      : false
                  }
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <EmailIcon />
                      </InputAdornment>
                    )
                  }}
                  onChange={e => handleChange(e)}
                />
                <TextField
                  required
                  fullWidth
                  className={classes.input}
                  type="password"
                  label="Password"
                  variant="outlined"
                  size="small"
                  name="password"
                  margin="normal"
                  error={error.password !== "" ? true : false}
                  value={state.password}
                  helperText={
                    error.password !== ""
                      ? "Password Must at least contain one Uppercase, one Lowercase character, one number and one special character"
                      : null
                  }
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <LockIcon />
                      </InputAdornment>
                    )
                  }}
                  onChange={e => handleChange(e)}
                />
                <TextField
                  required
                  fullWidth
                  className={classes.input}
                  type="password"
                  name="confirmPassword"
                  size="small"
                  variant="outlined"
                  label="Confirm Password"
                  margin="normal"
                  error={error.confirmPassword !== "" ? true : false}
                  value={state.confirmPassword}
                  helperText={
                    error.confirmPassword !== "" ? "Must Match Password" : null
                  }
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <LockIcon />
                      </InputAdornment>
                    )
                  }}
                  onChange={e => handleChange(e)}
                />
                <Button
                  color="primary"
                  fullWidth
                  className="loading"
                  variant="contained"
                  type="submit"
                >
                  Register
                </Button>
                <Typography className={classes.typography}>
                  Already have an account? <Link to="/login">Login</Link>
                </Typography>
              </div>
            </form>
          </Paper>
        </Grid>
      </Grid>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Error</DialogTitle>
        <DialogContent>
          <DialogContentText>{errorMessage}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default Register;
